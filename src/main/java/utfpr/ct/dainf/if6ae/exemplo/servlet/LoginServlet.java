package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Universidade Tecnológica Federal do Paraná
 * IF6AE Desenvolvimento de Aplicaçoões Web
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
@WebServlet(name = "Login Servlet", urlPatterns = {"/minhaacao"})
public class LoginServlet extends HttpServlet {

    /**
     * Processa requisições HTTP para o método
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
//    protected void processRequest(HttpServletRequest request,
//                                  HttpServletResponse response)
//            throws ServletException, IOException {
////        response.setContentType("text/html;charset=UTF-8");
////        if(request.getParameter("login").equals(request.getParameter("senha")))
////            response.sendRedirect("/sucesso?nome=" + request.getParameter("login") + "&senha=" + request.getParameter("senha"));
////        else
////            response.sendRedirect("/erro.xhtml");
//    }

    /**
     * Processa a requisição HTTP para o método
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        if(!request.getParameter("login").isEmpty() && 
                request.getParameter("login").equals(request.getParameter("senha")))
            response.sendRedirect("./sucesso?perfil=" + request.getParameter("perfil") + "&login=" + request.getParameter("login"));
        else
            response.sendRedirect("./erro.xhtml");
    }

    /**
     * Retorna uma descrição resumida do servlet.
     * @return Uma descrição resumida.
     */
    @Override
    public String getServletInfo() {
        return "Servlet que processa informações de login";
    }// </editor-fold>
}